![Calendar Events](../images/classic/20.calendar_events.png)

___
### Time Period

Set the time period for which you want your events to be displayed. This option filters **Past**, **Future** or **Today** events.

____
### Time Range

The time range sets the time frame within the time period for which events will be displayed.

___
### Event Limit

Choose the number of events you want to display on your calendar up to **25** events. By default, it will display 5.

___
### Highlight events when user is late

You can highlight events for which users are running late. These events will be highlighted in a light red. You can choose **NO** and do not highlight it, or to highlight it when the **Evenet started and ends in less than 1 hour** or **12 hours**.

___
### Highlight upcoming events

Here you can choose to highlight upcoming events when **Events starts in the next 24 hours** or when **Events starts in the next 48 hours**. These events will be highlighted in a light yellow.

___
### Recurrent events

Check this box if you want to hide recurrent events. By default, this option is disabled.

___

### All day events

Check this box if you want to hide all-day events. By default, this option is disabled.
