
1. Open the site where you have the Calendar web part installed;
2. Click **Bindtuning**, and then **Edit Web parts** to edit the web part;

	![view_calendar](../../images/classic/03.view_calendar.gif)

3. On the web part sidebar click the **View Calendar** icon.
4. The Calendar view will open with all your events;
5. If you want to delete an event, click on it and the select **Delete Event**, in the top menu.

    ![view_calendar](../../images/classic/26.edit_event_menu.png)