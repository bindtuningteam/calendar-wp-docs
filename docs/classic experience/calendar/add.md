1. Open the page where you've added the web part; 
2. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
3. Click on the ➕ (plus) icon to add a new **Event**;

	![view_calendar](../../images/classic/04.add_event.gif)


4. Fill out the form that pops up. You can check what you need to do in each section on the [Calendar](../../global/calendar);
5. After setting everything up, click on **Save**.

___
### Directly on the Calendar list

1. Open the settings menu and click on **Site Contents**;

	![sitecontents.png](https://bitbucket.org/repo/647zxpn/images/2287560858-sitecontents.png)

2. Look for your Calendar list and open it;

	![openMyCalendarlist.gif](https://bitbucket.org/repo/647zxpn/images/745558365-openMyCalendarlist.gif)

3. Then, search for the day where you want to add an event, and click on **Add**.

	![addcalendarevents.gif](https://bitbucket.org/repo/647zxpn/images/1263843632-addcalendarevents.gif)

4. Fill out the form that pops up. You can check out what you need to do in each setting in the [Calendar](../global/calendar) section of this User Guide;
5. After setting everything up, click on **Save**.