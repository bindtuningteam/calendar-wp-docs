![Messages](../images/modern/05.messages.png)

Decide how you would like the web part errors to be displayed. Three options are available:
 
- **Show In Edit Mode Only** - Error messages will be visible only if the page is in edit mode.
- **Always Show** - Error messages will always be visible to any user.
- **Never Show** - Error messages will never be visible.

___
### No Results Message

Decide how you would like the web part to be displayed when no results are possible to be retrieve or the Calendar doesn't have results. Three options are available:
 
- **Show In Edit Mode Only** - No result message be visible only if the page is in edit mode.
- **Always Show** - No result message will always be visible to any user.
- **Never Show** - No result message will never be visible.