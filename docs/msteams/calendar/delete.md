1. Open the Team on **Teams panel** that you intend to delete the Web Part; 
2. Click on the **Settings** button.

	![settings_delete.png](../../images/msteams/setting_edit.png)

3. Click on the **View Calendar** icon;

	![Manage](../../images/msteams/manage_calendar.png)

4. The Calendar view will open with all your events;

    ![view_calendar](../../images/modern/02.view_event.gif)

5. If you want to delete an event, click on it and the select **Delete Event**, in the top menu.

    ![view_calendar](../../images/classic/26.edit_event_menu.png)