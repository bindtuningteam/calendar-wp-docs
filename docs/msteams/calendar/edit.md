1. Open the Team on **Teams panel** that you intend to edit the Web Part; 
2. Click on the **Settings** button.

	![settings_delete.png](../../images/msteams/setting_edit.png)

3. Click on the **View Calendar** icon;

	![Manage](../../images/msteams/manage_calendar.png)
 
4. The Calendar view will open with all your events;
5. If you want to edit an event, click on it and the select **Edit Event**, in the top menu.

	![view_calendar](../../images/classic/26.edit_event_menu.png)
	
6. The item will be loaded and you can edit the item from the list.
7. You can chekout what you can do in each setting in the [Calendar](../global/calendar) section of this User Guide; 
8. Once you finish press to **Save**

    ![view_calendar](../../images/classic/25.save_event.png)
