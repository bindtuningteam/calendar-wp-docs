### Create a Calendar list

The first step of your web part configuration is creating a SharePoint Calendar. You will use this list to store your calendar events.

Let's create your list:

1. Open the settings menu and click on **Add an app**;

	![addanapp](../images/classic/17.addanapp.png)

2. On the search box type **Calendar**;
3. Look for the **Calendar list** and open it;
4. Pick a name for the app and click on **Create**;
5. You've created the list that will contain all the specific fields from this web part.

![addanapp](../images/classic/24.calendar_list.png)

_____

To your web part works, you need to choose between use **SharePoint Calendars** list that you have created or to use the **Search Results** option, that works with <a href="https://dev.office.com/sharepoint/docs/general-development/search-in-sharepoint">SharePoint Search</a>.

___
### SharePoint Calendars

![list_event](../images/modern/01.list_event.gif)


Now is time to select the list that you have created. In the first field, you can write your SharePoint site relative path or the list URL. In the second one, you should select the list that you want to use in this web part and click on the disk icon to save it. This is usefull because you can re-use a list that you've created before. Notice that is possible to add more than just one calendar list.


Without **Add Calendar**, the web part cannot function and you will see an error message: "OOPS. THIS WEB PART IS NOT FULLY CONFIGURED.".

Here are a few **recommendations** and **heads up**:

- Use relative paths for this field. So instead of using an URL like **https://company.sharepoint.com/sites/Home**, you should use something like **/sites/Home/**. This will ensure that the web part will work regardless of how you’re accessing the site.

___
### Search Results

![serachresults.png](../images/classic/23.calendar_search.png)

By choosing this option, you'll be able to:
1. **Search all of SharePoint**, if you want to search all the calendar events in all site collections (to wich you have access);  
2. **Search a site** if you to search all the events in a specific site. Here you need to write the **Site URL** in a field below the dropdown.
3. **Search a site collection** if you want to search all the events in a specific site collection.  Here you need to write the **Site URL** in a field below the dropdown.