![generaloptions](../images/modern/13.filters.png)
___
### Calendar Layout

Calendars can be displayed in different ways, by selecting different layouts. 5 layouts are available:

|**Layout 1**| **Layout 2**|**Layout 3**|
|-|-|-|
|![layout1.png](https://bitbucket.org/repo/647zxpn/images/1045305283-layout1.png)|![layout2.png](https://bitbucket.org/repo/647zxpn/images/3300130147-layout2.png)|![layout3.png](https://bitbucket.org/repo/647zxpn/images/396382424-layout3.png)|


|**Layout 4**|**Layout 5**|
|-|-|
|![layout4.png](https://bitbucket.org/repo/647zxpn/images/3953152094-layout4.png)|![layout5.png](https://bitbucket.org/repo/647zxpn/images/566425114-layout5.png)|

____
### Filter by categories (default)

You have the ability to filter your events by category, based on SharePoint preset categories: **Meeting, Work Hours, Business, Get Together, Gifts, Birthday, Anniversary**.

____
### Filter by categories (custom)

When choosing this option, you can filter the events based on your own categories.

____
### Filter by location

Imagine that you want to see only the events that are going to happen in London and Paris. In this field, you can filter your events by location. The location must match the one defined in each event.