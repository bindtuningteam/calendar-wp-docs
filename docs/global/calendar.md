![calendar_events](../images/classic/27.mycalendar.png)

<p class="alert alert-info">Here we show the default columns of the Calendar, you can create your own columns.</p>

### Title

This is the event title. This will show as an header in the calendar event layouts.

___
### Location

Here you can specify the event's location. It will appears below the title.

___
### Start Time

If you want to define an interval when your event will happen, this is the field to choose the start time.

___
### End Time

If you want to define an interval when your event will happen, this is the field to choose the end time.

____
### Description

A short description of the calendar event.

___
### Category

There are two options here: you can choose a SharePoint default category or you can choose one of the custom categories that you have created on **Web Part Properties Glossary**.

___
### All Day Event

Select this option in case that your event hasn't an hour to start or to end. Make it an all-day event.

___
### Recurrence

Select this option if you want to make your event a repeting event.


After setting everything up, click on **Save**.

Go back to the site where you have the Calendar web part installed and there it is, the event that you have created.

<p class="alert alert-info">If your event doesn't appears, check if it is included in the interval that you defined on <b>Time Period</b> and <b>Time Range</b> in the <b>Calendar Event</b>.</p>
