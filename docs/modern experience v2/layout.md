These sections allows customization of the visual aspects of the web part. It has the following settings:
![layout](../images/modernx/36.layout.png)

- [Color coding](#color-coding) - allow configuration of the color coding to be applied to the events.
- [Custom icons](#custom-icons) - allow configuration of the icon coding to be applied to the events.
- [Maximum Rows](#maximum-rows) - specifies the maximum number of rows to be shown before pagination is applied (from no maxiumum to 30 rows).
- [Date format](#date-format) - specifies the visual structure of any Date to be shown in the web part - has four diferent options.

### Color Coding
____
You have the ability to configure color coding based on a specific value of the selected column. The custom values for the coding can be added/removed and its value is defined by a textfield or a dropdown (if the selected column is a Choice type). Associated to each custom value there is also a color to be used, selected with a color picker.

![layoutcolorcoding](../images/modernx/37.layout_color_coding.png)
![layoutcolorcodingcolumn](../images/modernx/39.layout_coding_column.png)
![layoutcolorcodingadd](../images/modernx/40.layout_coding_add.png)
![layoutcolorcodingadded](../images/modernx/41.layout_color_added.png)
![layoutcolorcodingpicker](../images/modernx/42.layout_color_picker.png)

### Custom Icons
____
You have the ability to configure icon coding based on a specific value of the selected column. The custom values for the coding can be added/removed and its value is defined by a textfield or a dropdown (if the selected column is a Choice type). Associated to each custom value there is also a icon to be used, selected with an icon picker.

![layouticoncoding](../images/modernx/38.layout_icon_coding.png)
![layouticoncodingcolumn](../images/modernx/39.layout_coding_column.png)
![layouticoncodingadd](../images/modernx/40.layout_coding_add.png)
![layouticoncodingadded](../images/modernx/41.layout_icon_added.png)
![layouticoncodingpicker](../images/modernx/42.layout_icon_picker.png)