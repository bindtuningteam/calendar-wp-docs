![options](../images/modernx/00.properties.png)

On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [Events Source](../datasource)
- [Filter Events](../filters)
- [Layout](../layout)
- [Actions](../actions)
- [Search Filter](../searchFilter)
- [Theme](../theme)
- [Audience Targeting](../audiences)