![webpartapperence](../images/modern/06.webpartapperence.png)

### Theme Colors on the Title Bar

Enabling this option will make it so the web part's title bar inherits the background color from your theme or customization on the page. 
This option should help in maintaining branding consistency with native SharePoint web parts.

___
### Theme Colors on the WebPart Content

With this option **enable** will make your Links and the Date of events inherit the theme colors.
If you've configured the Web Part to have custom colors, it'll override the theme colors.

___
### Right to Left

Using this option will change the web part's text orientation to go from right to left. The forms will not be affected.