Choose audiences for the web part. The web part will be only visible to the selected users or groups.

![audiences](../images/modernx/19.audiences.png)