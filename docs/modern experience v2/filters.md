This section allows filtration of the events based on a number of factors:

![filters](../images/modernx/28.filterevents.png)

- [Time Period](#time-period) - specifies the period of the events (past, present, future, custom).
- [Time Range](#time-range) - specifies the maximum time range based on the time period (1 hour, 25 hours, 3 months, etc...).
- [Event limit](#event-limit) - specifies the maximum number of events to be retrieved (from no limit to 25).
- [Show recurrent events](#show-recurrent-events) - enables or disables recurrent events to be shown.
- [Show all day events](#show-all-day-events) - enables or disables all day events to be shown.
- [Filter by categories](#filter-by-categories) - filters the events shown to only events that match the specified categories.
- [Filter by location](#filter-by-location) - filters the events shown to only events that match the specified locations.

### Time Period
____
Regarding the time period, there are four options available:

![timeperiodoptions](../images/modernx/25.timeperiod.png)

- **Past** - filters the events shown to only past events.
- **Future** - filters the events shown to only future events.
- **Today** - filters the events shown to only events that start and/or end today.
- **Custom** - filters the events shown to only events that respect the selected custom interval.

![timeperiodoptionscustom](../images/modernx/26.timeperiod_custom.png)
![timeperiodoptionscustomdate](../images/modernx/27.timeperiod_custom_date.png)

### Time Period
____
Regarding the time range, the options are the following:

![timeperiod](../images/modernx/29.timerange.png)

### Filter by categories
____
You have the ability to filter your events by category, based on SharePoint preset categories and add your own filters based on custom categories. 
The default preset category values can be selected from a multi-select dropdown. The custom categories can be added/removed and its value is defined by a textfield.

![filtercat](../images/modernx/30.filtercategories.png)
![filtercatinside](../images/modernx/32.customcategorypane.png)
![filtercatinside](../images/modernx/34.catfilterscustom.png)
![filtercatinside](../images/modernx/35.catfilterscustomtoinsert.png)

### Filter by location
____

You have the ability to filter your events by location, based on your own custom locations. The custom locations can be added/removed and its value is defined by a textfield.

![filterloc](../images/modernx/31.filterlocation.png)
![filterlocinside](../images/modernx/33.filterlocationadd.png)
![filterlocinside](../images/modernx/33.filterlocationadd1.png)