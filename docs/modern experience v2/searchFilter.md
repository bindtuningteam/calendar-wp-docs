By allowing the following options, you will add search, filter and sort capabilities to the webpart.

<p class="alert alert-info">
The available fields differ depending if you are changing on search, filter or sort, or which data source you are connected to.
</p>

The three options use a dropdown to choose which fields the users can search, filter and sort from.

![search_filter_sort_dropdown](../images/modernx/45.search_filter.png)

___
### Search
This adds a search bar to the webpart.
You can choose which fields are available to search by through the dropdown on the settings panel.

![search](../images/modernx/32.search.png)

On the webpart, you can search in three different ways:

Match all words:

![match_all_words](../images/modernx/29.match_all_words.png)

Match any word:

![match_any_word](../images/modernx/30.match_any_word.png)

Match all words (cross fields):

![match_all_words_cross_fields](../images/modernx/31.match_all_words_cross_fields.png)

___
### Filter
This allows the users to filter the webpart items through the selected fields on the settings panel dropdown.

![filter](../images/modernx/34.filter.png)

You can select which type of filter you want, it can be:

Dropdown:

![filter_dropdown](../images/modernx/35.filter_dropdown.png)

Buttons:

![filter_buttons](../images/modernx/36.filter_buttons.png)

Panel:

![filter_panel](../images/modernx/37.filter_panel.png)