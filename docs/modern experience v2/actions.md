This section allows enabling/disabling of the calendar actions to the user when in publish mode:

![filters](../images/modernx/43.actions.png)

- [Go to source](#go-to-source) - button that allows direct access to the source of the events shown (Calendar, SharePoint List, Outlook).
- [Add events](#add-events) - button that allows direct access to adding a new event to the source.

### Go to source
____
![filters](../images/modernx/44.actions_goSource.png)

### Add events
____
![filters](../images/modernx/44.actions_add.png)
