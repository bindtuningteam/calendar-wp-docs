![data_source](../images/modernx/05.datasource.png)


The first step of your web part configuration is to connect to a data source. The available data sources are:
 
- [Events list](#events-list) - Get data from a SharePoint events lists
- [SharePoint list](#sharepoint-list) - Get data from custom SharePoint lists.
- [Microsoft 365 groups](#ms-365-groups) - Get data from MS 365 groups
- [Outlook](#outlook) - Get data from personal's outlook
- [Search](#search) - Get data from personal's outlook
</br>


### Events list
---

![events_list](../images/modernx/11.event_list_picker.png)

1. Choose the current site or another site
2. Create a new Events list or choose an existing one

On the cog icon, you can choose existing calendar overlays

![advanced_list_setup](../images/modernx/15.advanced_setup_lists.png)

![overlays](../images/modernx/16.overlays.png)


### SharePoint list
---

![custom_list](../images/modernx/14.sharepoint_list_picker.png)

1. Choose the current site or another site
2. Create a new Events list or choose any custom list that have dates

<p class="alert alert-warning">Then, you need to configure mappings for the custom list.</p> 

To configure the mappings, click on the warning icon

![custom_list](../images/modernx/18.mappings_configure.png)

![configure_mappings](../images/modernx/17.mappings.png)

Under **mapping options** choose the columns that you want map to the events fields. Start Datem End Date and Title are mandatory

### MS 365 groups
---

![ms365_groups](../images/modernx/12.group_picker.png)

<p class="alert alert-warning">To use this data source, some Web API permissions must be granted on your SharePoint admin page. The required permissions are Calendars.Read and Group.Read.All</p>

Here, you can choose the user's personal calendar and/or any MS 365 group calendar.

### Outlook
---

![outlook](../images/modernx/20.outlook.png)

<p class="alert alert-warning">To use this data source, some Web API permissions must be granted on your SharePoint admin page. The required permissions are Calendars.Read and Group.Read.All</p>

Using this data source, the web part will display the user's personal outlook calendar. 

### Search
---

![search](../images/modernx/21.search.png)

By choosing this option, you'll be able to filter the data:

![searchfilteroptions](../images/modernx/23.search_filters_options.png)

1. **Search all of SharePoint**, if you want to search all the calendar events in all site collections (to which you have access);  

2. **Search a site collection** if you want to search all the events in a specific site collection.  Here you use the site picker to select the sites.

![sitecollection](../images/modernx/24.search_filters_sitecollection.png)