1. Click on the **Edit** button;

    ![edit-page](../../images/modern/01.edit.modern.png)

2. On the web part click the **View Calendar** icon;
4. The Calendar view will open with all your events;

![view_calendar](../../images/modern/02.view_event.gif)

5. If you want to delete an event, click on it and the select **Delete Event**, in the top menu.

    ![view_calendar](../../images/classic/26.edit_event_menu.png)