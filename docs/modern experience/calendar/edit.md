1. Click on the **Edit** button;

	![edit-page](../../images/modern/01.edit.modern.png)

2. On the web part click the **View Calendar** icon;

	 ![view_calendar](../../images/modern/02.view_event.gif)
	 
4. The Calendar view will open with all your events;
5. If you want to edit an event, click on it and the select **Edit Event**, in the top menu.

	![view_calendar](../../images/classic/26.edit_event_menu.png)
	
6. The item will be loaded and you can edit the item from the list.
7. You can chekout what you can do in each setting in the [Calendar](../global/calendar) section of this User Guide; 
8. Once you finish press to **Save**

    ![view_calendar](../../images/classic/25.save_event.png)
