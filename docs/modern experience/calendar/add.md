### Connected on List Picker

1. Click on the **Edit** button;

	![edit-page](../../images/modern/01.edit.modern.png)

2. Mouse hover the Web Part and click on the **[+]** button to add a new **Event**
4. Fill out the form that pops up. You can check what you need to do in each section on the [Calendar](../../global/calendar);
5. After setting everything up, click on **Save**.

	![add_event](../../images/modern/02.add_event.gif)

___
### Directly on the Calendar list

1. Open the settings menu and click on **Site Contents**;

	![Site_contents](../../images/classic/28.site_contents.png)

2. Look for your Calendar list and open it;

	![openMyCalendarlist.gif](https://bitbucket.org/repo/647zxpn/images/745558365-openMyCalendarlist.gif)

3. Then, search for the day where you want to add an event, and click on **Add**.

	![addcalendarevents.gif](https://bitbucket.org/repo/647zxpn/images/1263843632-addcalendarevents.gif)

4. Fill out the form that pops up. You can check out what you need to do in each setting in the [Calendar](../global/calendar) section of this User Guide;
5. After setting everything up, click on **Save**.