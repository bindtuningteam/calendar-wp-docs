![options](../images/modern/07.options.png)

On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [Event Source](./list)
- [Calendar Filters](./filters)
- [Calendar Events](./event)
- [Category Settings](./category)
- [Web Part Appearance](./appearance)
- [Advanced Options](./advanced)
- [Performance](./performance)
- [Web Part Messages](./message)